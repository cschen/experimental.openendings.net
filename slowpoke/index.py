#!/usr/bin/python

import sys
import time

print "Content-type: image/png"
print ""

with open("slowpoke.png", "rb") as f:
	while True:
		chunk = f.read(1024)
		if not chunk:
			break
		sys.stdout.write(chunk)
		time.sleep(1)
